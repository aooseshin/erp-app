
var $tableAction = $('.table-action');

if ($tableAction) {
  $.each($tableAction, function(index, chk) {
    var $table = $(chk);
    var selectIndex = $table.data('selectPos');
    if (selectIndex) {
      var $selectAll = $table.find(`thead th:nth-child(${selectIndex}) input[type="checkbox"]`)
      var selectShow = $table.data('selectShow');
      $table.on('click', `thead th:nth-child(${selectIndex}) input[type="checkbox"]`, function(ev) {
        var $selectChkItem = $table.find(`tbody td:nth-child(${selectIndex}) input[type="checkbox"]`)
        var checked = ev.target.checked;
        $.each($selectChkItem, function(index, chk) {
          chk.checked = checked;
        });

        if (selectShow) {
          updateShow();
        }
      });
      
      $table.on('click', `tbody td:nth-child(${selectIndex}) input[type="checkbox"]`, function(ev) {
        var $selectChk = $table.find(`tbody td:nth-child(${selectIndex}) input[type="checkbox"]`);
        var selectCount = $selectChk.filter((index) => {
          return $selectChk[index].checked
        }).length;
        if (selectCount && selectCount === $selectChk.length) {
          $selectAll[0].checked = true;
        } else {
          $selectAll[0].checked = false;
        }
        if (selectShow) {
          updateShow();
        }
      });

      function updateShow() {
        var $el = $(selectShow);
        var $parent = $el.parents('.top-action-body');
        var $selectChk = $table.find(`tbody td:nth-child(${selectIndex}) input[type="checkbox"]`)
        var selectCount = $selectChk.filter((index) => {
          return $selectChk[index].checked
        }).length;
        $el.html(selectCount);
        if (selectCount) {
          $parent.addClass('has-select-count');
          $parent.find('.need-count-active').attr('disabled', false);
        } else {
          $parent.removeClass('has-select-count');
          $parent.find('.need-count-active').attr('disabled', true);
        }
      }
    }
  });
}

$('.sortable a').click(function(ev) {
  addSortClass(this);
});

$('.sortable .in-sort').click(function(ev) {
  addSortClass(this);
  var $parent = $(this).parents('.sortable');
  var index = $parent.index();
  var sort = $parent.hasClass('sort-desc') ? 0 : 1; 

  sortTable(this, index, sort);

});

function addSortClass(el) {
  var $this = $(el);
  var $sortable = $this.parent();
  var $parent = $sortable.parent();
  let currentClass = $sortable.hasClass('sort-desc') ? 'sort-asc' : 'sort-desc';
  $parent.find('.sortable')
    .removeClass('sort-desc')
    .removeClass('sort-asc');
  $sortable.addClass(currentClass);
}


function sortTable(el, col, sortOrder) {
  var $table = $(el).parents('table');
  var $tbody = $table.find('tbody');
  var rows = $table.find('tbody >tr:has(td)').get();
  rows.sort(function (a, b) {
    var val1 = $(a).children('td').eq(col).text().toUpperCase();
    var val2 = $(b).children('td').eq(col).text().toUpperCase();
    if ($.isNumeric(val1) && $.isNumeric(val2))
      return sortOrder === 1 ? val1 - val2 : val2 - val1;
    else
      return (val1 < val2) ? -sortOrder : (val1 > val2) ? sortOrder : 0;
  })

  $.each(rows, function (index, row) {
    $tbody.append(row);
  });
}
