import TableExport from 'tableexport';

var $exportArea = $('.export-area');

$.each($exportArea, function(index, area) {
  var $area = $(area);
  var formats = $area.data('formats').split(',');
  var date = new Date();
  var dateString = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`;
  var tableName = $area.data('tableName') || dateString;
  var id = $(area).find('table')[0].id;

  var instance = new TableExport(document.getElementById(id), {
    formats,
    exportButtons: false,
    sheetname: dateString, 
    filename: tableName,
  });

  formats.forEach(format => {
    var exportData = instance.getExportData()[id][format];
    var button = $(`#${id}`).parent().find(`.btn-${format}`)[0];
    button.addEventListener('click', function (e) {
      instance.export2file(exportData.data, exportData.mimeType, exportData.filename, exportData.fileExtension);
    });
  });
})
