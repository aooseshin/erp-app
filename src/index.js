import './scss/main.scss'
import './js/menu';
import './js/table';
import './js/export';
import 'bootstrap';

$('[data-toggle-class]').click(function() {
  var klass = $(this).data('toggleClass');
  var target = $(this).data('toggleTarget');
  if (target) {
    $(target).toggleClass(klass)
  } else {
    $(this).toggleClass(klass)
  }
})

$('[data-toggle-only]').click(function() {
  var klass = $(this).data('toggleOnly');  
  $(this).parent().find(`.${klass}`).removeClass(klass);
  $(this).addClass(klass)
})


function checkForInput(element) {
  var $parent = $(element).parent();
  if ($(element).val().length > 0) {
    $parent.addClass('input-has-value');
  } else {
    $parent.removeClass('input-has-value');
  }
}

$('.has-clear').find('input').on('change keyup blur', function() {
  checkForInput(this);  
});

$('.has-clear').find('i').on('click', function() {
  $(this).prev().val('');
  checkForInput(this);  
});