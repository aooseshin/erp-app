const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const devMode = process.env.NODE_ENV !== 'production';
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  entry: {
    app: ['./src/index.js'],
  },
  plugins: [
    new CopyWebpackPlugin([
      { from: 'src/assets', to: 'assets' }
    ]),
    new CleanWebpackPlugin(['dist']),
    new MiniCssExtractPlugin({
      filename: "[name].[chunkhash].css",
      chunkFilename: "[id].[chunkhash].css"
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/index.pug',
      filename:'./index.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/login.pug',
      filename:'./login.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/alert.pug',
      filename:'./alert.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/inventory.pug',
      filename:'./inventory.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/products.pug',
      filename:'./products.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/product.pug',
      filename:'./product.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/orders.pug',
      filename:'./orders.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/advertisement/product.pug',
      filename:'./advertisement/product.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/purchasing/draft.pug',
      filename:'./purchasing/draft.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/fba.pug',
      filename:'./fba.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/setting/setting.pug',
      filename:'./setting/index.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/setting/account/list.pug',
      filename:'./setting/account/index.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/setting/account/show.pug',
      filename:'./setting/account/show.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/setting/account/edit.pug',
      filename:'./setting/account/edit.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/setting/account/create.pug',
      filename:'./setting/account/create.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/setting/sop/list.pug',
      filename:'./setting/sop/index.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/setting/sop/show.pug',
      filename:'./setting/sop/show.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/setting/sop/edit.pug',
      filename:'./setting/sop/edit.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/setting/sop/create.pug',
      filename:'./setting/sop/create.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/setting/staff/list.pug',
      filename:'./setting/staff/index.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/setting/staff/show.pug',
      filename:'./setting/staff/show.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/setting/staff/edit.pug',
      filename:'./setting/staff/edit.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/setting/staff/create.pug',
      filename:'./setting/staff/create.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/setting/vendor/list.pug',
      filename:'./setting/vendor/index.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/setting/vendor/show.pug',
      filename:'./setting/vendor/show.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/setting/vendor/edit.pug',
      filename:'./setting/vendor/edit.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/setting/vendor/create.pug',
      filename:'./setting/vendor/create.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/setting/warehouse/list.pug',
      filename:'./setting/warehouse/index.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/setting/warehouse/show.pug',
      filename:'./setting/warehouse/show.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/setting/warehouse/edit.pug',
      filename:'./setting/warehouse/edit.html',
      chunks: ['app']
    }),
    new HtmlWebpackPlugin({
      template: './src/views/pages/setting/warehouse/create.pug',
      filename:'./setting/warehouse/create.html',
      chunks: ['app']
    }),
    new webpack.ProvidePlugin({
      'tableDragger': 'tableDragger'
    }),
  ],
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /.js$/,
        exclude: /node_modules/,
        use: ['babel-loader'],
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          { loader: 'css-loader', options: { importLoaders: 1 } },
          {
            loader: 'postcss-loader',
            options: {
              plugins: (loader) => [
                require('autoprefixer')()
              ]
            }
          },
          'sass-loader',
        ]
      },
      {
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: 'images/[name].[ext]'
            }
          }
        ]
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/,
        use: [
          'file-loader'
        ]
      },
      {
        test: /\.pug$/,
        use: [
          {
            loader: 'html-loader',
            options: {
              minimize: false
            }
          },
          {
            loader: 'pug-html-loader',
            options: {
              pretty: '  '
            }
          }
        ]
      }
    ]
  }
};
